//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
//naprednih (sin, cos, log, sqrt...) operacija.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lv6analizazad1
{
    public partial class Form1 : Form
    {
        double num1, num2;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            if (!double.TryParse(tB_2.Text, out num2))
                MessageBox.Show("Pogrešan unos druge znamenke");
            result.Text = (num1 - num2).ToString();
            operation.Text = ("-");
        }

        private void divide_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            if (!double.TryParse(tB_2.Text, out num2))
                MessageBox.Show("Pogrešan unos druge znamenke");
            result.Text = (num1 / num2).ToString();
            operation.Text = ("/");
        }

        private void multiply_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            if (!double.TryParse(tB_2.Text, out num2))
                MessageBox.Show("Pogrešan unos druge znamenke");
            result.Text = (num1 * num2).ToString();
            operation.Text = ("*");
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            result.Text = Math.Sin(num1).ToString();
            operation.Text = ("sin(num1)");
        }

        private void cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            result.Text = Math.Cos(num1).ToString();
            operation.Text = ("cos(num1)");
        }

        private void tan_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            result.Text = Math.Tan(num1).ToString();
            operation.Text = ("tan(num1)");
        }

        private void ctan_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            result.Text = Math.Atan(num1).ToString();
            operation.Text = ("ctg(num1)");
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            result.Text = Math.Sqrt(num1).ToString();
            operation.Text = ("sqrt(num1)");
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_1.Text, out num1))
                MessageBox.Show("Pogrešan unos prve znamenke");
            if (!double.TryParse(tB_2.Text, out num2))
                MessageBox.Show("Pogrešan unos druge znamenke");
            result.Text = (num1 + num2).ToString();
            operation.Text = ("+");
        }
    }
}
